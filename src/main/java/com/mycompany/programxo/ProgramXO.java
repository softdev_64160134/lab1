/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.programxo;

import java.util.Scanner;

/**
 *
 * @author USER
 */
public class ProgramXO {
private Player player1, player2;
    private Board board;

    public static void main(String[] args) {
        ProgramXO p = new ProgramXO();
        p.startGame();
    }

    public void startGame() {
        Scanner sc = new Scanner(System.in);
        player1 = playerInput(1);
        player2 = playerInput(2);

        //players input
        while (player1.getSymbol() == player2.getSymbol()) {
            System.out.println("Symbol Already taken !! Pick another symbol !!");
            System.out.print("Please select a new symbol: ");
            char symbol = sc.next().charAt(0);
            player2.setSymbol(symbol);
        }

        //create board
        board = new Board(player1.getSymbol(), player2.getSymbol());

        //Conduct the Game
        boolean p1Turn = true;
        int status = Board.incomplete;
        while (status == Board.incomplete || status == Board.invalid) { //continue when status is Board.incomplete or Board.invalid
            if (p1Turn) {
                System.out.println("Player 1: " + player1.getName() + " turn");
                System.out.print("Enter row: ");
                int row = sc.nextInt();
                System.out.print("Enter col: ");
                int col = sc.nextInt();
                status = board.move(player1.getSymbol(), row, col);
                if (status != Board.invalid) { //post the same position and go p2Turn
                    p1Turn = false;
                    board.print();
                } else {
                    System.out.println("Invalid move try again!!!");
                }
            } else {
                System.out.println("Player 2: " + player2.getName() + " turn");
                System.out.print("Enter row: ");
                int row = sc.nextInt();
                System.out.print("Enter col: ");
                int col = sc.nextInt();
                status = board.move(player2.getSymbol(), row, col);
                if (status != Board.invalid) {
                    p1Turn = true;
                    board.print();
                } else {
                    System.out.println("Invalid move try again!!!");
                }
            }
        }
        if (status == Board.p1_win) {
            System.out.println("Player 1 - " + player1.getName() + " wins !!");
        } else if (status == Board.p2_win) {
            System.out.println("Player 2 - " + player2.getName() + " wins !!");
        } else {
            System.out.println("Draw !!");
        }
        playAgain();
    }

    public void playAgain() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Do you want to play again? (Y/N): ");
        char choice = sc.next().charAt(0);
        if (Character.toUpperCase(choice) == 'Y') {
            startGame();
        } else {
            System.out.println("Thank you for playing. Goodbye!");
        }
    }

    private Player playerInput(int num) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Player " + num + " name: ");
        String name = sc.nextLine();
        System.out.print("Enter Player " + num + " symbol: ");
        char symbol = sc.next().charAt(0);
        Player p = new Player(name, symbol);
        return p;

    }
}
